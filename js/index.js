const overlay = document.querySelector('.overlay'),
         auth = document.querySelector('.auth'),
        close = document.querySelector('.close');

auth.addEventListener('click',()=>{
  overlay.classList.add('act')
})

close.addEventListener('click',()=>{
  overlay.classList.remove('act')
})

//tabs
const tabs = document.querySelector('.section__tabs'),
      tab  = document.querySelectorAll('.section__tab'),
     block = document.querySelectorAll('.block-section');

function showTab(i = 0) {
  block[i].classList.remove('hide')
  block[i].classList.add('show')
}

function hideTab() {
  block.forEach(tab =>{
    tab.classList.remove('show')
    tab.classList.add('hide')
  })
}

function addTab() {
  tabs.addEventListener('click', (e) => {
    if (e.target && e.target.classList.contains('section__tab')) {
      hideTab();
      tab.forEach((tab, i) => {
        if (e.target === tab) {
          showTab(i)
        }
      })
    }
  })
}
hideTab();
showTab();
addTab();